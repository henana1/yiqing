//接口地址
const key='6d19a4af9935a289f367c257e2136964'//天行疫情信息接口key
const base={
    host:'https://api.inews.qq.com',//腾讯疫情域名
    coninfo:'https://apis.tianapi.com/ncov/index?key='+key,//天行数据-疫情信息接口
    newsWorld:'/newsqa/v1/automation/modules/list?modules=FAutoCountryConfirmAdd,WomWorld,WomAboard',//海外疫情
    china:'/api/china',//中国疫情
}
export default base