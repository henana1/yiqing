//请求的方法
import base from "./base"
import axios from "../request/request"
const api={
    // 疫情信息接口
    // get请求
    //key:6d19a4af9935a289f367c257e2136964
    getConInfo(){
        return axios.get(base.coninfo)
    },
    //海外疫情
    getNewsWorld(){
       return axios.post(base.host+base.newsWorld)
    },
    //中国疫情
    getChina(){
        return axios.get(base.china);
    },
}
export default api