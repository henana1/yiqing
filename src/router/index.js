import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/home/HomeView.vue'
import EdgePage from '../views/EdgePage.vue'
Vue.use(VueRouter)

const routes = [
  {
   path:'/',
   component:EdgePage,
  //  children:[
  //   {
  //     path:'/',
  //     name:'home',
  //     component:HomeView
  //   },
  //   {
  //     path:'/',
  //     component:()=>import('../views/guowai/OverSea.vue')
  //   }
  //  ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
