import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './vant'
import './assets/css/reset.css'


Vue.config.productionTip = false
import api from './api'
Vue.prototype.$api=api;
//导入echrats 4.x
// import echarts from 'echarts'
//5.x
import *as echarts from 'echarts'
Vue.prototype.$echarts=echarts
//导入echarts插件
import echarts2 from './plugins/echarts'
Vue.use(echarts2)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
