const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8989',
        changeOrigin: true,
        pathRewrite:{
          '^/api':''
        }
      },
      '/foo': {
        target: 'http://www.baidu.com'
      }
    }
  },
})
